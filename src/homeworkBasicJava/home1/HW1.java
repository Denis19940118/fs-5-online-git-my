package src.homeworkBasicJava.home1;

import java.util.Scanner;

public class HW1 {
    public static void main(String[] args) {
        int[] wrongNum = new int[99];
        int index = 0;
        int randomNum = (int)(Math.random() * (100 + 1));
        System.out.println("Let the game begin!");
        System.out.println("please enter your name:");
        Scanner scap = new Scanner(System.in);
        String name = scap.nextLine();
        System.out.println("Your name: " + name);
        for (int i = 0; i < 100; i++) {
            System.out.println("please enter a number");
            if (!scap.hasNextInt()) {
                System.err.println("Error, please enter a number");
            }
            int userNum = scap.nextInt();
            System.out.println("Your a number: " + userNum);

            if (randomNum == userNum) {
                System.out.printf("Congratulations, %s!", name);
                return;
            } else if(randomNum < userNum){
                System.out.println("Your number is too big. Please, try again.");
                wrongNum[index++] = userNum;
            } else if(randomNum > userNum){
                System.out.println("Your number is too small. Please, try again.");
                wrongNum[index++] = userNum;
            }
        }
    }
}
