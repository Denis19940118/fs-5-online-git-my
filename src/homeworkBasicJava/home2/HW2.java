package homeworkBasicJava.home2;

import java.util.Scanner;

public class HW2 {
    public static void main(String[] args) {
        int[][] matrixGame = new int[6][6];
        int randomLine = (int)Math.floor(1 + Math.random() * 5);
        int randomCol = (int)Math.floor(1 + Math.random() * 5);
        matrixGame[randomLine][randomCol] = 6;
        System.out.println(randomLine);
        System.out.println(randomCol);

        System.out.println("All Set. Get ready to rumble!");

        Scanner scap = new Scanner(System.in);
        int initial = 0;
        for (int k = 0; initial < 1; k++) {
            System.out.println("please enter a number line for fire from 1 to 5");
            if (!scap.hasNextInt()) {
                System.err.println("Error, please enter a number line for fire from 1 to 5");
            }

            int lineUser = scap.nextInt();
            if (lineUser > 5 || lineUser < 1) {
                System.err.println("Error, please enter a number for fire from 1 to 5");
            }
            System.out.println("please enter a number column for fire");

            int columnUser = scap.nextInt();
            if (columnUser > 5 || columnUser < 1) {
                System.err.println("Error, please enter a number column for fire from 1 to 5");
            }

            if (lineUser <= 5 && lineUser >= 1 && columnUser <= 5 && columnUser >= 1) {
                for (int i = 0; i < matrixGame.length; i++) {
                    System.out.print('[');
                    for (int j = 0; j < matrixGame[i].length; j++) {
                        if(i == 0){
                            System.out.print(j);
                            if (j < matrixGame[i].length - 1) {
                                System.out.print(", ");
                            }
                        }
                        if(j == 0 && i != 0) {
                            System.out.print(i);
                            System.out.print(", ");
                        }
                        if(i > 0 && j != 0){
                            if (matrixGame[i][j] == 6 && matrixGame[lineUser][columnUser] == 6) {
                                System.out.print("x");
                                matrixGame[i][j] = 10;
                                if (j < matrixGame[i].length - 1) {
                                    System.out.print(", ");
                                }
                            }
                            if (matrixGame[i][j] != 6 && matrixGame[i][j] != 10 && lineUser == i || matrixGame[i][j] == 7) {
                                if (columnUser == j || matrixGame[i][j] == 7) {
                                    System.out.print("*");
                                    matrixGame[i][j] = 7;
                                    if (j < matrixGame[i].length - 1) {
                                        System.out.print(", ");
                                    }
                                } else {
                                    System.out.print("-");
                                    if (j < matrixGame[i].length - 1) {
                                        System.out.print(", ");
                                    }
                                }
                            } else {
                                if (matrixGame[i][j] != 10) {
                                    System.out.print("-");
                                    if (j < matrixGame[i].length - 1) {
                                        System.out.print(", ");
                                    }
                                }
                            }
                        }
                    }
                    System.out.println(']');
                }
                if (matrixGame[lineUser][columnUser] == 10) {
                    System.out.println("You have won!");
                    return;
                }
            }
        }
    }
}
