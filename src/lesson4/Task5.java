package lesson4;

import java.util.Scanner;
// Пользователь вводит строку
// Пользователь вводит строку заменитель
// В оригинальной строке заментить все чётные символы на строку заменитель и вывести на экран
// abba
// ccc
// cccbccca
public class Task5 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter word or message");
        String word = scan.nextLine();
        System.out.println("Please enter world symbols ");
        String symbols = scan.nextLine();
        String worldEdit = "";

        for (int i = 0; i < word.length(); i++) {
            if(i%2 == 0){
                worldEdit = worldEdit + symbols;
            }
            else {
                worldEdit = worldEdit + word.charAt(i);
            }
        }
        System.out.println(worldEdit);
    }
}