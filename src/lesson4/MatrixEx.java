package lesson4;

import java.util.Arrays;

public class MatrixEx {
    public static void main(String[] args) {
//        [1,2,3,4,5]

//        [1,2,3]
//        [4,5,6]
//        [7,8,9]

        int[][] matrix = new int[3][3];
        System.out.println(Arrays.deepToString(matrix));
        matrix[0][0] = 1;
        matrix[2][2] = 5;
        System.out.println(Arrays.deepToString(matrix));

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.println(matrix[i][j]);
            }
        }

        int[][] mat= {
            {1, 2, 3},
            {4},
            {7, 8, 9}
        };
        mat = null;
        for (int[] line: mat) {
            System.out.print('[');
            for (int i = 0; i < line.length; i++) {
                System.out.print(line[i]);
                if (i < line.length -1){
                    System.out.print(", ");
                }
            }
            System.out.println(']');
        }
    }
}
