package lesson4;

import java.util.Arrays;
import java.util.Scanner;
// Пользователь вводит размер двумерного массива
// Заполнить случайными значениями [0,9]
// Расположить строки двумерного массива в обратном порядке и вывести на экран
// 3
// 3
// [6, 2, 4]
// [2, 4, 5]
// [7, 3, 9]
// [7, 3, 9]
// [2, 4, 5]
// [6, 2, 4]
public class Task3 {
    public static void main(String[] args) {
        System.out.println("Please enter length matrix");
        System.out.println("Please enter height matrix");
        Scanner scan = new Scanner(System.in);
        if(!scan.hasNextInt()){
            System.err.println("Please enter a number");
            System.exit(0);
        }
        int height = scan.nextInt();
        System.out.println("Please enter width matrix");
        int width = scan.nextInt();
        int[][] matrixes = new int[height][width];

        for (int i = 0; i < matrixes.length; i++) {
            for (int j = 0; j < matrixes[i].length; j++) {
                matrixes[i][j] = (int)(Math.random() * (9 + 1));
            }
        }

        for (int[] line: matrixes ) {
            System.out.print('[');
            for (int i = 0; i < line.length; i++) {
                System.out.print(line[i]);
                if (i < line.length -1){
                    System.out.print(", ");
                }
            }
            System.out.println(']');
        }
        System.out.println();

        int[] tempLine;
        for (int i = 0; i < matrixes.length/2; i++) {
            tempLine = matrixes[i];
            matrixes[i] = matrixes[matrixes.length - i - 1];
            matrixes[matrixes.length - i - 1] = tempLine;
        }

        for (int[] line: matrixes ) {
            System.out.println(Arrays.toString(line));
        }
    }
}
