package lesson4;
// Пользователь вводит строку
// Пользовать вводит символ
// Вывести на экран количество вхождений символа в строку
// "abba"
// 'b'
// 2
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        System.out.println("Please enter word or message");
        Scanner scan = new Scanner(System.in);
        String word = scan.nextLine();
        System.out.println("Please enter letter ");
        char letter = scan.nextLine().charAt(0);
        int count = 0;
        for (int i = 0; i < word.length(); i++) {
            if(word.charAt(i) == letter){
                count++;
            }
        }
        System.out.println(letter);
        System.out.println(count);

    }
}
