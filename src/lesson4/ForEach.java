package lesson4;

import java.util.Arrays;

public class ForEach {
    public static void main(String[] args) {
        int[] vector = {1,3,1};
//        Arrays.fill(vector,0);
        Arrays.sort(vector);
//        System.out.println(Arrays.toString(vector));
        for (int element: vector) {
            System.out.println(element + " ");
            System.out.print(element + " ");
        }
    }
}
