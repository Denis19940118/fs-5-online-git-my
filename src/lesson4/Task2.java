package lesson4;
// Пользователь вводит размер двумерного массива
// Заполнить массив возрастающей последовательностью чисел начиная с 1
// Заполнить змейкой
// 3
// 3
// [1, 2, 3]
// [6, 5, 4]
// [7, 8, 9]
import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        System.out.println("Please enter length matrix");
        System.out.println("Please enter height matrix");
        Scanner scan = new Scanner(System.in);
        if(!scan.hasNextInt()){
            System.err.println("Please enter a number");
            System.exit(0);
        }
        int height = scan.nextInt();
        System.out.println("Please enter width matrix");
        int width = scan.nextInt();
        int[][] matrixes = new int[height][width];
        int num = 1;
        for (int i = 0; i < matrixes.length; i++) {
            for (int j = 0; j < matrixes[i].length; j++) {
                if(i % 2 == 0){
                    matrixes[i][j] = num++;
                }
                else {
                    matrixes[i][matrixes[i].length - j - 1] = num++;
                }
            }
        }

        for (int[] line: matrixes ) {
            System.out.println(Arrays.toString(line));
        }
    }
}
