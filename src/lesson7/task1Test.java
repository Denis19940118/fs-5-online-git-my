package src.lesson7;
import lesson7.Task1;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class task1Test {
    @Test
    public void testMinLengthOfMatrix() {
        int[][] matrix = {
                {1, 2, 3},
                {1, 2}
        };
        int result = Task1.getMinLineLength(matrix);
        assertEquals(2, result);
    }

    @Test
    public void testMinLengthOfEmptyMatrix() {
        int[][] matrix = {{}};
        int result = Task1.getMinLineLength(matrix);
        assertEquals(0, result);
    }

    @Test
    public void testMinLengthOfNullAsMatrix() {
        int[][] matrix = null;
        int result = Task1.getMinLineLength(matrix);
        assertEquals(0, result);
    }

    @Test
    public void testMinLengthOfMatrixOfPossibleNulls() {
        int[][] matrix = {{1, 2, 3}, null};
        int result = Task1.getMinLineLength(matrix);
        assertEquals(0, result);
    }

    @Test
    public void testMinLengthOfMatrixOfPossibleEmpty() {
        int[][] matrix = {{1, 2, 3}, {}};
        int result = Task1.getMinLineLength(matrix);
        assertEquals(0, result);
    }
}