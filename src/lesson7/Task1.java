package lesson7;

public class Task1 {
    public static void main(String[] args) {
        int[][] matrix = {
                {1,2,3},
                {1,2}
        };
        int result = getMinLineLength(matrix);
        System.out.println(result);
    }

    public static int getMinLineLength(int[][] matrix){
        int minLineLength = Integer.MAX_VALUE;

        if(matrix == null){
            return 0;
        }

        for (int[] array: matrix) {
            if(array == null){
                return 0;
            }
            if(array.length<minLineLength){
                minLineLength = array.length;
            }
        }
        return minLineLength;
    }
}
