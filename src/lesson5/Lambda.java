package lesson5;

public class Lambda {
    public static void main(String[] args) {
        Function fun = ()->{
            System.out.println("");
        };
        fun.apply();
    }
}
interface Function{
    void apply();
}
