package lesson5;

public class ClassEx {
    public static void main(String[] args) {
        int value = 3;
//        Person person = new Person();
//        person.age = 28;
//        person.inn = "9876543123";
//        person.name = "Gleb";
//        System.out.printf(" %s, %d, %s", person.name, person.age, person.inn);
//        Person person = new Person("Gleb", 28, "9876543123");
//        person.say();

//        Person person2 = new Person();
//        person2.age = 18;
//        person2.inn = "9876788123";
//        person2.name = "Jora";
//        System.out.printf(" %s, %d, %s", person2.name, person2.age, person2.inn);
//        Person person2 = new Person("Jora",18,"9876788123");
//        person2.say();

        Person person = new Person();
    }
}

class Person{
    String name;
    int age;
    String inn;

//    public  Person(){
//
//    }
    public void say(){
        System.out.printf(" %s, %d, %s", name, age, inn);
    }
//    public Person(String name, int age, String inn){
//        this.name = name;
//        this.age = age;
//        this.inn = inn;
//    }
}
