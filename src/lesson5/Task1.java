package lesson5;

import java.util.Scanner;
// Написать метод переворота строки
// 123
// 321
public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter your string");
        String str = scan.nextLine();
        System.out.println(stringReverse(str));
        String resReverse = stringReverse(str);
        System.out.println(resReverse);
        System.out.println(stringBuilderReverse(str));
        System.out.println(stringBufferReverse(str));
    }
    public static String stringReverse(String str){
        String currentSymbol = "";
        for (int i = str.length() - 1; i >= 0; i--) {
            currentSymbol = currentSymbol + String.valueOf(str.charAt(i));
        }
        return currentSymbol;
    }
    public static String stringBufferReverse(String str){
        StringBuffer stringBuffer = new StringBuffer(str);
        stringBuffer.reverse();
        return stringBuffer.toString();
    }
    public static String stringBuilderReverse(String str) {
        return new StringBuilder(str).reverse().toString();
    }
}
