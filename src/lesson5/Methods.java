package lesson5;

import java.util.Arrays;

public class Methods {
    public static void main(String[] args) {
//        printSquare();
//        printSquare();
//        printSquare();
        printLine(15);
        int value = 1;
        mutate(value);
        System.out.println(value);
        String str = "1";
        mutate(str);
        System.out.println(str);
        int[] vector = {1};
        mutate(vector);
        System.out.println(Arrays.toString(vector));
        int res = add(3,3);
        System.out.println(res);
    }
    public  static int add(int first, int second){
        int res = first + second;
        return res;
    }
    public static void mutate(int[] vector){
        vector[0] = 2;
    }
    public static void mutate(String str){
        str = str + "2";
    }
    public static void mutate(int value){
        value++;
    }
    public static void printLine(int size){
        for (int i = 0; i < size; i++) {
            System.out.print("*");
        }
    }
    public static void printSquare(){
        System.out.println("******");
        System.out.println("******");
        System.out.println("******");
        System.out.println("******");
        System.out.println("******");
    }
}
