package lesson3;

import java.util.Arrays;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter length string, please");

        if(!scan.hasNextInt()){
            System.err.println("Enter please a number");
            System.exit(0);
        }

        int length = scan.nextInt();
        for (int i = 0; i < length; i++) {
            System.out.print("* = " + i + ", ");
        }
    }
}
