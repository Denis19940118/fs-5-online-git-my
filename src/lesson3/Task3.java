package lesson3;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter 1 border square");

        if (!scan.hasNextInt()){
            System.err.println("Enter please a number");
            System.exit(0);
        }

        int line = scan.nextInt();
        for (int i = 0; i < line; ++i) {
            for (int j = 0; j <= i; j++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }
}
