package lesson3;

public class WhileEx {
    public static void main(String[] args) {
        int i = 0;
        while (i < 5){
            if ( i == 3){
                i++;
                continue;
            }
            if ( i == 3){
                i++;
                break;
            }
            System.out.print(i + " ");
            i++;
        }
        outer:for (int j = 0; j < 5; j++) {
            for (int k = 0; k < 5; k++) {
                if (k == 2){
                    break outer;
                }
            }
        }

        int k = 0;
        do {
            System.out.println("iterate");
            k++;
        }while (k < 5);
    }
}
