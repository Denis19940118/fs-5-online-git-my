package lesson3;

import java.util.Arrays;
import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter please length array");
        if(!scan.hasNextInt()){
            System.out.println("Enter please a number");
            System.exit(0);
        }

        int size = scan.nextInt();

        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * (10 + 1));
        }
        System.out.println("arr :" + Arrays.toString(arr));
    }
}
