package lesson3;

import java.util.Arrays;
import java.util.Scanner;

public class Task6 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter please length array");
        if(!scan.hasNextInt()){
            System.out.println("Enter please a number");
            System.exit(0);
        }

        int size = scan.nextInt();

        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Enter please a second number");
            if(!scan.hasNextInt()){
                System.out.println("Enter please a number");
                System.exit(0);
            }
            arr[i] = scan.nextInt();
        }
//        int[] arr1 = new int[size];
//        arr1[arr.length] = arr[i];
        int arrfirst;
        for (int i = 0; i < arr.length/2 ; i++) {
            arrfirst = arr[i];
            arr[i] = arr[arr.length - 1];
            arr[arr.length - 1] = arrfirst;

        }
        System.out.println("arr :" + Arrays.toString(arr));
    }
}
