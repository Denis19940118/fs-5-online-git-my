package lesson2;

import java.util.Scanner;
// Пользователь вводит номер месяца в году, врнуть количество дней в месяце
public class Task4 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter number month: ");
        if(!scan.hasNextInt()){
            System.err.println("Sorry your enter data is not number");
        }
        int month = scan.nextInt();
        if(month > 12 || month < 1) {
            System.err.println("Sorry, you entered incorrect number :( ");
            System.exit(0);
        }
        int days = switch (month){
            case 1,4,6,8,10,12 -> 31;
            case 3,5,7,11,9 -> 30;
//            case 2 -> 28;
//            case 1 -> 31;
//            case 2 -> 28;
//            case 3 -> 30;
//            case 4 -> 31;
//            case 5 -> 30;
//            case 6 -> 31;
//            case 7 -> 30;
//            case 8 -> 30;
//            case 9 -> 31;
//            case 10 -> 30;
//            case 11 -> 30;
//            case 12 -> 31;
            default -> -1;
        };
        System.out.println( "Days in the enter month " + days);
    }
}
