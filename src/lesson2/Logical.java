package lesson2;

public class Logical {
    public static void main(String[] args) {
//         & | !
        boolean andRes = true & false;
        int age = 30;
        boolean applicable = age > 18 && age < 65;
        boolean free = age < 18 || age > 65;
        applicable = !free;
    }
}
