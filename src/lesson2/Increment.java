package lesson2;

public class Increment {
    public static void main(String[] args) {
        int var;
        var = 5;
        int next = var + 1;

        var += 1;
        var = -var;
        var++;
        var--;
        int res = --var + ++var;
        ++var;
        --var;
    }
}
