package lesson2;

import java.util.Scanner;

public class Task3 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Input number day from week");

        String res;
        if(!scan.hasNextInt()) {
            System.err.println("Sorry, you most enter a number [1-7] ");
            System.exit(0);
        }
        int num = scan.nextInt();

        if(num > 7 || num < 1) {
            System.err.println("Sorry, you entered incorrect number :( ");
            System.exit(0);
        }
        if(num == 1){
            res = "Manday";
        }
        else if(num == 2){
            res = "Tuesday";
        }
        else if(num == 3){
            res = "Wednesday";
        }
        else if(num == 4){
            res = "Thursday";
        }
        else if(num == 5){
            res = "Friday";
        }
        else if(num == 6){
            res = "Saterday";
        }
        else {
            res = "Sanday";
        }

        System.out.println("Your day " + res);
    }
}
