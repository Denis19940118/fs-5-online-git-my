package src.lesson7_1;

public final class FinalEx {
    private final String ID;
    public FinalEx(String ID) {
        this.ID = ID;
    }
    public final static void main(String[] args) {
        final int MAX_VAL = 200;
//        MAX_VAL = 100;
        Animal animal = new Animal("Predator",1, 123);
        animal.color();
        System.out.println();
    }
}

record Animal(String type, int weight, int color){

}

//class New extends FinalEx{
//
//}