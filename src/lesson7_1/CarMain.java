package src.lesson7_1;

public class CarMain {
    public static void main(String[] args) {
        Motor motor = new Motor();
        Car car = new Car(motor,"Daewoo");
//        car.force();
        System.out.println(car);
    }
}

class Car {
    private Motor motor;
    private String model;

    public Car(Motor motor, String model){
        this.motor = motor;
        this.model = model;
    }
    @Override
    public String toString(){
        return "Car{" +
                "motor=" + motor +
                ", model=" + model + '\'' +
                '}';
    }
}

class Motor {
    private double volume;
    private double rotationSpeed;
    public void force(){
        rotationSpeed += 100;
    }

    @Override
    public String toString(){
        return "Car{" +
                "motor=" + volume +
                ", model=" + rotationSpeed + '\'' +
                '}';
    }
}
