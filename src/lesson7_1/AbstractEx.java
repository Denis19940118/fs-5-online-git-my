package src.lesson7_1;

public class AbstractEx {
    public static void main(String[] args) {
//        AbstractPerson person = new AbstractPerson();
        Speakable realPerson = new RealPerson();
        if(realPerson instanceof AbstractPerson) {
            AbstractPerson abstractPerson = (AbstractPerson) realPerson;
        }
        realPerson.say();
        Speakable frog = new Frog();
        realPerson.say();

    }
}
interface Speakable {
    //    public static final
    int MAX = 2000;
    //    public abstract
    void say();
    void speak();

    static void go(){

    }
}
class Frog implements Speakable {
    @Override
    public void say(){
        System.out.println("Qua-qua");
    }

    @Override
    public void speak() {

    }
}
abstract class AbstractPerson implements Speakable {
    private String name;
    public AbstractPerson(){

    }
    public abstract void say();
}

//class RealPerson extends AbstractPerson {
//    @Override
//    public void say(){
//
//    }
//}

class RealPerson extends AbstractPerson /*implements Speakable, Runnable*/ {
    @Override
    public void say() {

    }

    @Override
    public void speak() {

    }

//    @Override
//    public void speak() {
//
//    }
//
//    @Override
//    public void run(){
//
//    }
}

interface  Writable extends  Speakable, Runnable{
    void write();
}