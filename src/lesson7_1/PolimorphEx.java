package src.lesson7_1;

public class PolimorphEx {
    public static void main(String[] args) {
        Vehicle vehicle = new Vehicle();
        Car[] cars =
                {new Car(null,null),new Car(null,"aa")};
        Speakable[] speakables = {new Frog(), new RealPerson()};
        vehicle.speak(speakables);
        vehicle.equals(new String());
    }
}

class Vehicle{
    int value;

//    @Override
//    public boolean equals(Object obj) {
//        return false;
//    }
    public boolean speak(Speakable[] speakables) {
        for (Speakable speakable: speakables) {
            speakable.say();
        }
        return false;
    }
    public boolean equals(Vehicle obj) {
        return false;
    }
}