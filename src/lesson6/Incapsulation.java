package lesson6;

public class Incapsulation {
    public static void main(String[] args) {
//        Car car = new Car();
//        car.model = "Zaz";
//        car.year = 1997;
//        car.setModel("Zaz");
//        car.setYear(1997);
    }
}

//class Car {
//    private String model;
//    private int year;
//
//    public void setModel(String model){
//        this.model = model;
//    }
//
//    public String getModel() {
//        return model;
//    }
//
//    public void setYear(int year) {
//        this.year = year;
//    }
//    public int getYear() {
//        return year;
//    }
//}
