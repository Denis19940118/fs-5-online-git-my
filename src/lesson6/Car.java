package lesson6;

import java.util.Objects;

// Создать класс Car (model, year, use, price)
// Создать конструкторы, реализовать инкапсуляцию, переопределить
// equals, toString
public class Car {
    private String model;
    private int year;
    private boolean use;
    private double price;

    public Car(){
        model = "";
        year = 0;
        use = false;
        price = 0;
    }
    public Car(String model, int year, boolean use, double price){
        this.model = model;
        this.year= year;
        this.use = use;
        this.price = price;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setUse(boolean use) {
        this.use = use;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public boolean isUse() {
        return use;
    }

    public double getPrice() {
        return price;
    }
    @Override
    public String toString(){
        return String
                .format("Car { Model: %s, Year: %d, Use: %b, Price: %.2f",
                model, year, use, price);
    }
    @Override
    public boolean equals(Object object) {
        if (object == null){
            return false;
        }
        if (this == object) {
            return true;
        }
        Car car = (Car) object;
        if ( car.getClass() != Car.class){
            return false;
        }
        if(this.model == null && car.getModel() == null){
            return true;
        }
        return price == car.getPrice() && model.equals(car.getModel())
                && year == car.getYear() && use == car.isUse();

    }

}
