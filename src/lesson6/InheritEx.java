package lesson6;

public class InheritEx {
    public static void main(String[] args) {
//        Doctor doc1 = new Doctor("House", 22);
//        Doctor doc2 = new Doctor("House", 22);
        Person doc1 = new Person("House");
        Person doc2 = new Person("House");
//        doc.setName("Chaos");
        System.out.println(doc2);
        System.out.println(doc1.equals(doc2));
//        doc.equals(new Doctor());
    }
}

class Person{
    private String name;
    public Person(){
        super();
    }
    public Person(String name){
         this.name = name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
//    @Override;
//    public String toString(int val){
//        return "";
//    }
    @Override
    public String toString(){
        return "Person{"+
                "Nane = " + this.name +
                "}";
    }
    @Override
    public boolean equals(Object obj){
        if (this == obj){
            return true;
        }
        if (obj == null){
            return false;
        }
        if (obj.getClass() != Person.class){
            return false;
        }
        Person person = (Person)obj;
        if (this.name == null || person.name == null){
            return false;
        }
        return this.name.equals(person.getName());
    }
    @Override
    public int hashCode(){
       return this.name == null ? 0 : this.name.hashCode();
    }
}

class Doctor extends Person {
    private int experience;
//    public Doctor() {
//        super();
//    }
    public Doctor() {
        super("Default");
    }
    public Doctor (String name, int experience){
        super(name);
    }
    @Override
    public String toString(){
        return "Doctor{"+
                "Nane = " + this.getName() +
                "Experiance = " + this.experience +
                "}";
    }
}
